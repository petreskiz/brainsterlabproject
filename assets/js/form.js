let kopce = document.querySelector("#kopceZaNadole");
let button = document.querySelector(".btn-form");

kopce.addEventListener("click", () => {
  button.classList.toggle("zzz");
});

button.addEventListener("click", function() {
  document.getElementById("myForm").submit();
});

function izberiStudent(text) {
  button.classList.toggle("zzz");
  document.querySelector("#student").innerHTML = text;
  let input = document.querySelector("#student-tip");
  if (text === "Студенти од маркетинг") {
    input.value = "marketing";
  }
  if (text === "Студенти од програмирање") {
    input.value = "programiranje";
  }
  if (text === "Студенти од data science") {
    input.value = "datascience";
  }
  if (text === "Студенти од дизајн") {
    input.value = "dizajn";
  }
}
