let toggle = document.querySelector(".toggle");
let sidebar = document.querySelector(".sidebar");
let esc = document.querySelector(".esc");
toggle.addEventListener("click", () => {
  sidebar.style.width = "100%";
});

esc.addEventListener("click", () => {
  sidebar.style.width = "0";
});