let mark = document.getElementById("mark");
let prog = document.getElementById("prog");
let diz = document.getElementById("diz");
let cards = document.querySelectorAll(".cards-holder");

function displayCards(cartickite, pokazi) {
  for (let i = 0; i < cartickite.length; i++) {
    const element = cartickite[i];
    element.style.display = pokazi;
  }
}

function filterCards(akademja) {
  displayCards(cards, "none");
  mark.classList.remove("active");
  prog.classList.remove("active");
  diz.classList.remove("active");

  if (akademja == "marketing") {
    let marketing = document.querySelectorAll(".marketing");
    mark.classList.add("active");
    displayCards(marketing, "block");
  } else if (akademja == "programiranje") {
    let development = document.querySelectorAll(".programiranje");
    prog.classList.add("active");
    displayCards(development, "block");
  } else if (akademja == "dizajn") {
    let dizajn = document.querySelectorAll(".dizajn");
    diz.classList.add("active");
    displayCards(dizajn, "block");
  }
}

mark.addEventListener("click", () => {
  filterCards("marketing");
});

prog.addEventListener("click", () => {
  filterCards("programiranje");
});

diz.addEventListener("click", () => {
  filterCards("dizajn");
});

for (let i = 0; i < cards.length; i++) {
  const element = cards[i];
  element.addEventListener("click", function() {
    location.href = "#";
  });
}
document.querySelector(".btn-sidebar").addEventListener("click", function() {
  location.href =
    "/home/zlatko/Desktop/Project01-ZlatkoPetreskiF2/vraboti-student.html";
});
